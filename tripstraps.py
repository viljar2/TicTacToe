# Tic-Tac-Toe game in Python

# Initialize the tictactoeboard
tictactoeboard = [' ' for _ in range(9)]

# Function to display the Tic-Tac-Toe tictactoeboard


def display_board(tictactoeboard):
    print(tictactoeboard[0] + '|' + tictactoeboard[1] + '|' + tictactoeboard[2])
    print('-+-+-')
    print(tictactoeboard[3] + '|' + tictactoeboard[4] + '|' + tictactoeboard[5])
    print('-+-+-')
    print(tictactoeboard[6] + '|' + tictactoeboard[7] + '|' + tictactoeboard[8])

# Function to check for a win


def check_win(tictactoeboard, player):
    # Check rows
    for i in range(0, 9, 3):
        if tictactoeboard[i] == tictactoeboard[i + 1] == tictactoeboard[i + 2] == player:
            return True
    # Check columns
    for i in range(3):
        if tictactoeboard[i] == tictactoeboard[i + 3] == tictactoeboard[i + 6] == player:
            return True
    # Check diagonals
    if tictactoeboard[0] == tictactoeboard[4] == tictactoeboard[8] == player or tictactoeboard[2] == tictactoeboard[4] == tictactoeboard[6] == player:
        return True
    return False

# Function to check for a draw


def check_draw(tictactoeboard):
    return ' ' not in tictactoeboard

# Main game loop


current_player = 'X'
while True:
    display_board(tictactoeboard)
    position = int(input(f"Player {current_player}, enter a position (1-9): ")) - 1

    if tictactoeboard[position] == ' ':
        tictactoeboard[position] = current_player
        if check_win(tictactoeboard, current_player):
            display_board(tictactoeboard)
            print(f"Player {current_player} wins!")
            break
        elif check_draw(tictactoeboard):
            display_board(tictactoeboard)
            print("It's a draw!")
            break
        else:
            current_player = 'O' if current_player == 'X' else 'X'
    else:
        print("That position is already taken. Try again.")

# End of the game
